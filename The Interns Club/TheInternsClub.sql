DROP DATABASE if exists TheInternsClub;

CREATE DATABASE TheInternsClub;

USE TheInternsClub;

CREATE TABLE `TheInternsClub`.`student` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `school` VARCHAR(45) NULL,
  `first` VARCHAR(45) NOT NULL,
  `last` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NULL,
  `pathPicture` VARCHAR(256) NULL,
  `pathResume` VARCHAR(256) NULL,
  `bio` VARCHAR(1000) NULL,
  PRIMARY KEY (`username`, `password`, `first`, `last`));

CREATE TABLE `TheInternsClub`.`professor` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `school` VARCHAR(45) NULL,
  `first` VARCHAR(45) NOT NULL,
  `last` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NULL,
  `pathPicture` VARCHAR(256) NULL,
  `bio` VARCHAR(1000) NULL,
  PRIMARY KEY (`username`, `password`, `first`, `last`));

CREATE TABLE `TheInternsClub`.`employer` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `company` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NULL,
  `position` VARCHAR(100) NULL, 
  `pathLogo` VARCHAR(256) NOT NULL,
  `bio` VARCHAR(1000) NULL,
  PRIMARY KEY (`username`, `password`, `company`, `pathLogo`));

