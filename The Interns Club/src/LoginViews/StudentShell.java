package LoginViews;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import client.StudentProfile;

public class StudentShell extends JFrame {
	JPanel main;
	JToolBar Studentshell;
	JButton homeButton;
	JButton uploadButton;
	JButton viewButton;
	JButton logoutButton;
	JButton notification;
	BufferedImage homeImage;
	StudentProfile studentProfile;

	public StudentShell() {
		super("The Interns Club");
		setSize(500, 500);
		setLocation(100, 100);
		setResizable(false);
		initialization();
	}

	public void initialization() {
		main = new JPanel();
		studentProfile = new StudentProfile();
		main.add(studentProfile);
		main.setBackground(Color.white);
		Studentshell = new JToolBar();
		// Empshell.setBackground(Color.white);
		Studentshell.setFloatable(false);
		ImageIcon homeimage = new ImageIcon(this.getClass().getResource("home.png"));
		Image dum = homeimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		homeimage = new ImageIcon(dum);
		homeButton = new JButton(homeimage);
		homeButton.setPreferredSize(new Dimension(50, 30));
		homeButton.setToolTipText("Home");
		ImageIcon uploadimage = new ImageIcon(this.getClass().getResource("upload.png"));
		Image dum1 = uploadimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		uploadimage = new ImageIcon(dum1);
		uploadButton = new JButton(uploadimage);
		uploadButton.setPreferredSize(new Dimension(50, 30));
		uploadButton.setToolTipText("Upload Resume");
		ImageIcon viewimage = new ImageIcon(this.getClass().getResource("view.png"));
		Image dum2 = viewimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		viewimage = new ImageIcon(dum2);
		viewButton = new JButton(viewimage);
		viewButton.setPreferredSize(new Dimension(50, 30));
		viewButton.setToolTipText("View Jobs");
		ImageIcon logoutimage = new ImageIcon(this.getClass().getResource("logout.png"));
		Image dum3 = logoutimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		logoutimage = new ImageIcon(dum3);
		logoutButton = new JButton(logoutimage);
		logoutButton.setPreferredSize(new Dimension(50, 30));
		logoutButton.setToolTipText("Log out");
		ImageIcon notifimage = new ImageIcon(this.getClass().getResource("notif.png"));
		Image dum4 = notifimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		notifimage = new ImageIcon(dum4);
		notification = new JButton(notifimage);
		notification.setPreferredSize(new Dimension(50, 30));
		notification.setToolTipText("Notification");
		CreateGui();
	}

	public void CreateGui() {
		Studentshell.add(homeButton);
		Studentshell.add(Box.createHorizontalStrut(10));
		Studentshell.add(uploadButton);
		Studentshell.add(Box.createHorizontalStrut(10));
		Studentshell.add(viewButton);
		Studentshell.add(Box.createHorizontalStrut(10));
		Studentshell.add(notification);
		Studentshell.add(Box.createGlue());
		Studentshell.add(logoutButton);
		add(Studentshell, BorderLayout.NORTH);
		add(main, BorderLayout.CENTER);
		setVisible(true);
	}

	public void actionlisteners() {

	}
}
