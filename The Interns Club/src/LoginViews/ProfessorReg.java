package LoginViews;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ProfessorReg extends JFrame {
	private static final long serialVersionUID = 1L;
	JLabel email, fname, lname, school, profilePicture, resume, phone, professor, rq1, rq2, rq3, requiredKey, iFName, shortBioLabel;
	JTextField schoolField, emailField, fNameField, lNameField, phoneField, profName;
	JButton upload1, finish;
	JPanel mainPanel, gridPanel, flow0, flow1, flow2, flow3, flow4, flow5, flow6, flow7, flow8;
	JScrollPane jsp;
	JTextArea shortBio;
	
	public ProfessorReg() {
		super("Professor Registration");
		setSize(600,500);
		setLocation (500,500);
		initializeVariables();
		createGUI();
		addListeners();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void initializeVariables() {
		email = createLabel("Email (@.edu)");
		fname = createLabel("First Name");
		lname = createLabel("Last Name");
		phone = createLabel("Phone number");
		school = createLabel("School Affiliation");
		profilePicture = createLabel("Profile Picture");
		resume = createLabel("Resume");
		phone = createLabel("Office Phone");
		iFName = createLabel("(Image Name here)");
		requiredKey = createLabel("* = Required field");
		shortBioLabel = createLabel("Short Bio");
		rq1 = createLabel(" *");
		rq2 = createLabel(" *");
		rq3 = createLabel(" *");
		
		mainPanel = new JPanel();
		gridPanel = new JPanel(new GridLayout(8,1));
		
		flow0 = new JPanel();
		flow0.setLayout(new FlowLayout());
		flow1 = new JPanel();
		flow1.setLayout(new FlowLayout());
		flow2 = new JPanel();
		flow2.setLayout(new FlowLayout());
		flow3 = new JPanel();
		flow3.setLayout(new FlowLayout());
		flow4 = new JPanel();
		flow4.setLayout(new FlowLayout());
		flow5 = new JPanel();
		flow5.setLayout(new FlowLayout());
		flow6 = new JPanel();
		flow6.setLayout(new FlowLayout());
		flow7 = new JPanel();
		flow7.setLayout(new FlowLayout());
		flow8 = new JPanel();
		flow8.setLayout(new FlowLayout());
		
		
		shortBio = new JTextArea(3,20);
		jsp = new JScrollPane(shortBio);
		upload1 = createButton("Upload");
		finish = createButton("Finish");
		
		
		emailField = new JTextField();
		emailField.setPreferredSize(new Dimension(150,20));
		fNameField = new JTextField();
		fNameField.setPreferredSize(new Dimension(120,20));
		lNameField = new JTextField();
		lNameField.setPreferredSize(new Dimension(120,20));
		schoolField = new JTextField();
		schoolField.setPreferredSize(new Dimension(150,20));
		phoneField = new JTextField();
		phoneField.setPreferredSize(new Dimension(150,20));
		profName = new JTextField();
		profName.setPreferredSize(new Dimension(150,20));
		
	}
	
	public void createGUI() {
		add(mainPanel);
		
		flow0.add(requiredKey);
		gridPanel.add(flow0);
		
		flow1.add(school);
		flow1.add(schoolField);
		flow1.add(rq3);
		gridPanel.add(flow1);
		
		flow2.add(email);
		flow2.add(emailField);
		flow2.add(rq2);
		gridPanel.add(flow2);
		
		
		flow3.add(fname);
		flow3.add(fNameField);
		flow3.add(lname);
		flow3.add(lNameField);
		gridPanel.add(flow3);
		
		
		flow4.add(profilePicture);
		flow4.add(upload1);
		flow4.add(iFName);
		gridPanel.add(flow4);
		
		flow5.add(phone);
		flow5.add(phoneField);
		gridPanel.add(flow5);
		
		flow6.add(shortBioLabel);
		flow6.add(jsp);
		gridPanel.add(flow6);
		
		flow7.add(finish);
		gridPanel.add(flow7);
		
		mainPanel.add(gridPanel);
		
		
		
		
	}
	
	public void addListeners() {
		
	}
	
	public JButton createButton(String x) {
		JButton tempButton = new JButton(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempButton.setFont(font);
		return tempButton;
	}
	
	public JLabel createLabel(String x) {
		JLabel tempLabel = new JLabel(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempLabel.setFont(font);
		return tempLabel;
	}
	
	
	public static void main(String args[]) {
		ProfessorReg pr = new ProfessorReg();
		
	}
}
