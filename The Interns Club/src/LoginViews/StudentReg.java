package LoginViews;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class StudentReg extends JFrame {
	private static final long serialVersionUID = 1L;
	JLabel email, fname, lname, school, profilePicture, resume, phone, professor, rq1, rq2, rq3, requiredKey, iFName1, iFName2;
	JTextField emailField, fNameField, lNameField, schoolField, phoneField, profName;
	JButton upload1, upload2, finish, addButton;
	JPanel mainPanel, gridPanel, flow0, flow1, flow2, flow3, flow4, flow5, flow6, flow7, flow8;
	
	public StudentReg() {
		super("Student Registration");
		setSize(600,400);
		setLocation (500,500);
		initializeVariables();
		createGUI();
		addListeners();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void initializeVariables() {
		email = createLabel("Email (@.edu)");
		fname = createLabel("First Name");
		lname = createLabel("Last Name");
		phone = createLabel("Phone number");
		school = createLabel("School Affiliation");
		profilePicture = createLabel("Profile Picture");
		resume = createLabel("Resume");
		phone = createLabel("Phone Number");
		professor = createLabel("Professor");
		iFName1 = createLabel("(Image Name here)");
		iFName2 = createLabel("(Resume Name here)");
		requiredKey = createLabel("* = Required field");
		rq1 = createLabel(" *");
		rq2 = createLabel(" *");
		rq3 = createLabel(" *");
		
		mainPanel = new JPanel();
		gridPanel = new JPanel(new GridLayout(9,1));
		addButton = createButton("Add Professor");
		
		flow0 = new JPanel();
		flow0.setLayout(new FlowLayout());
		flow1 = new JPanel();
		flow1.setLayout(new FlowLayout());
		flow2 = new JPanel();
		flow2.setLayout(new FlowLayout());
		flow3 = new JPanel();
		flow3.setLayout(new FlowLayout());
		flow4 = new JPanel();
		flow4.setLayout(new FlowLayout());
		flow5 = new JPanel();
		flow5.setLayout(new FlowLayout());
		flow6 = new JPanel();
		flow6.setLayout(new FlowLayout());
		flow7 = new JPanel();
		flow7.setLayout(new FlowLayout());
		flow8 = new JPanel();
		flow8.setLayout(new FlowLayout());
		
		
		upload1 = createButton("Upload");
		upload2 = createButton("Upload");
		finish = createButton("Finish");
		
		
		emailField = new JTextField();
		emailField.setPreferredSize(new Dimension(150,20));
		fNameField = new JTextField();
		fNameField.setPreferredSize(new Dimension(120,20));
		lNameField = new JTextField();
		lNameField.setPreferredSize(new Dimension(120,20));
		schoolField = new JTextField();
		schoolField.setPreferredSize(new Dimension(150,20));
		phoneField = new JTextField();
		phoneField.setPreferredSize(new Dimension(150,20));
		profName = new JTextField();
		profName.setPreferredSize(new Dimension(150,20));
		
	}
	
	public void createGUI() {
		add(mainPanel);
		
		flow0.add(requiredKey);
		gridPanel.add(flow0);
		
		
		flow1.add(email);
		flow1.add(emailField);
		flow1.add(rq1);
		gridPanel.add(flow1);
		
		
		flow2.add(fname);
		flow2.add(fNameField);
		flow2.add(lname);
		flow2.add(lNameField);
		gridPanel.add(flow2);
		
		
		flow3.add(school);
		flow3.add(schoolField);
		flow3.add(rq3);
		gridPanel.add(flow3);
		
		
		flow4.add(profilePicture);
		flow4.add(upload1);
		flow4.add(iFName1);
		gridPanel.add(flow4);
		
		flow5.add(resume);
		flow5.add(upload2);
		flow5.add(iFName2);
		gridPanel.add(flow5);
		
		flow6.add(phone);
		flow6.add(phoneField);
		gridPanel.add(flow6);
		
		flow7.add(professor);
		flow7.add(profName);
		flow7.add(addButton);
		gridPanel.add(flow7);
		
		flow8.add(finish);
		gridPanel.add(flow8);
		
		mainPanel.add(gridPanel);
		
		
		
		
	}
	
	public void addListeners() {
		
	}
	
	public JButton createButton(String x) {
		JButton tempButton = new JButton(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempButton.setFont(font);
		return tempButton;
	}
	
	public JLabel createLabel(String x) {
		JLabel tempLabel = new JLabel(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempLabel.setFont(font);
		return tempLabel;
	}
	
	
	public static void main(String args[]) {
		StudentReg sr = new StudentReg();
		
	}
}

