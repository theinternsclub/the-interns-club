package LoginViews;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class EmployerReg extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel companyName, posAtComp, phone, description, companyLogo, rq1, rq2, rq3, rq4, rq5, requiredKey, iFName;
	JTextField compNameField, posAtCompField, phoneField;
	JTextArea descriptionArea;
	JButton upload, finish;
	JPanel mainPanel, gridPanel, flow1, flow2, flow3, flow4, flow5, flow6, flow0;;
	JScrollPane jsp;
	public EmployerReg() {
		super("Employer Registration");
		setSize(600,500);
		setLocation (500,500);
		initializeVariables();
		createGUI();
		addListeners();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void initializeVariables() {
		companyName = createLabel("Company Name");
		posAtComp = createLabel("Position at Company");
		phone = createLabel("Phone number");
		description = createLabel("Description of Company");
		companyLogo = createLabel("Company Logo");
		requiredKey = createLabel("* = Required field");
		iFName = createLabel("(Image Name goes here)");
		rq1 = createLabel(" *");
		rq2 = createLabel(" *");
		rq3 = createLabel(" *");
		rq4 = createLabel(" *");
		rq5 = createLabel(" *");

		mainPanel = new JPanel();
		gridPanel = new JPanel(new GridLayout(7,1));
		
		flow1 = new JPanel();
		flow1.setLayout(new FlowLayout());
		flow2 = new JPanel();
		flow2.setLayout(new FlowLayout());
		flow3 = new JPanel();
		flow3.setLayout(new FlowLayout());
		flow4 = new JPanel();
		flow4.setLayout(new FlowLayout());
		flow5 = new JPanel();
		flow5.setLayout(new FlowLayout());
		flow6 = new JPanel();
		flow6.setLayout(new FlowLayout());
		flow0 = new JPanel();
		flow0.setLayout(new FlowLayout());
		
		upload = createButton("Upload");
		finish = createButton("Finish");
		compNameField = new JTextField();
		compNameField.setPreferredSize(new Dimension(150,20));
		posAtCompField = new JTextField();
		posAtCompField.setPreferredSize(new Dimension(150,20));
		phoneField = new JTextField();
		phoneField.setPreferredSize(new Dimension(150,20));
		descriptionArea = new JTextArea(3,20);
		jsp = new JScrollPane(descriptionArea);
		
	}
	
	public void createGUI() {
		add(mainPanel);
		
		flow0.add(requiredKey);
		gridPanel.add(flow0);
		
		flow1.add(companyName);
		flow1.add(compNameField);
		flow1.add(rq1);
		gridPanel.add(flow1);
		
		flow2.add(posAtComp);
		flow2.add(posAtCompField);
		flow2.add(rq2);
		gridPanel.add(flow2);
		flow3.add(phone);
		flow3.add(phoneField);
		
		gridPanel.add(flow3);
		
		flow4.add(description);
		flow4.add(jsp);
		flow4.add(rq4);
		gridPanel.add(flow4);
		
		flow5.add(companyLogo);
		flow5.add(upload);
		flow5.add(iFName);
		flow5.add(rq5);
		gridPanel.add(flow5);
		
		flow6.add(finish);
		gridPanel.add(flow6);
		mainPanel.add(gridPanel);
		
		
		
		
	}
	
	public void addListeners() {
		
	}
	
	public JButton createButton(String x) {
		JButton tempButton = new JButton(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempButton.setFont(font);
		return tempButton;
	}
	
	public JLabel createLabel(String x) {
		JLabel tempLabel = new JLabel(x);
		Font font = new Font("Verdana", Font.BOLD, 12);
		tempLabel.setFont(font);
		return tempLabel;
	}
	
	public static void main(String args[]) {
		EmployerReg er = new EmployerReg();
		
	}
}
