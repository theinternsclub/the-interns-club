package LoginViews;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class JobDescriptionPanel extends JPanel{
	
	JLabel namethepostiton;
	JLabel jobdescription;
	JTextField jobnamefield;
	JTextArea jobdescriptionarea;
	JScrollPane jobdescriptionpane;
	JButton submit;
	JPanel dummy1;
	JPanel dummy2;
	JPanel dummy3;
	JPanel dummy4;
	JPanel dummy5;

	
	public JobDescriptionPanel(){
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		initialization();
	}
	public void initialization(){
		
		dummy1 = new JPanel();
		dummy2 = new JPanel();
		dummy3 = new JPanel();
		dummy4 = new JPanel();
		dummy5 = new JPanel();
		
		
		namethepostiton = new JLabel("Name of Position",  SwingConstants.LEFT );

			
		jobdescription = new JLabel("Job Descrioption",  SwingConstants.RIGHT);
		jobdescription.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		
		jobnamefield = new JTextField();
		jobnamefield.setPreferredSize(new Dimension(400,43));
		jobnamefield.setMaximumSize(new Dimension(400,43));
		jobnamefield.setMinimumSize(new Dimension(400,43));
		jobnamefield.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		jobdescriptionarea = new JTextArea();
		jobdescriptionarea.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		jobdescriptionpane = new JScrollPane(jobdescriptionarea);
		jobdescriptionpane.setPreferredSize(new Dimension(400,200));
		jobdescriptionpane.setMaximumSize(new Dimension(400,200));
		jobdescriptionpane.setMinimumSize(new Dimension(400,200));
		jobdescriptionpane.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		
		submit = new JButton("Submit");
		submit.setPreferredSize(new Dimension(160,60));
		submit.setMaximumSize(new Dimension(160,60));
		submit.setMinimumSize(new Dimension(160,60));
		submit.setAlignmentX(Component.LEFT_ALIGNMENT);
		createGui();
	}
	
	public void createGui(){
		add(dummy1);
		dummy1.add(namethepostiton);
		
		
		add(dummy2);
		jobnamefield.setAlignmentX(Component.CENTER_ALIGNMENT);
		dummy2.add(jobnamefield);
		add(Box.createVerticalStrut(35));
		
		add(dummy3);
		jobdescription.setAlignmentX(Component.RIGHT_ALIGNMENT);
		dummy3.add(jobdescription);
		
		add(dummy4);
		jobdescriptionpane.setAlignmentX(Component.CENTER_ALIGNMENT);
		dummy4.add(jobdescriptionpane);
		add(Box.createGlue());
		
		
		add(dummy5);
		submit.setAlignmentX(Component.LEFT_ALIGNMENT);
		dummy5.add(submit);
	}
	
	public void actionlisteners(){
		
	}
}
