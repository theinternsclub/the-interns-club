package LoginViews;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class ClientGui extends JFrame{
	
	JButton studentlogin;
	JButton gueststudentlogin;
	JButton professorlogin;
	JButton employerlogin;
	JPanel mainpanel;
	JLabel loginlabel;
	BufferedImage image;
	public ClientGui(){
		super("Welcome");
		setSize(360,720);
		setLocation(100,100);
		this.setResizable(false);
		mainpanel = new JPanel();
		mainpanel.setBackground(Color.white);
		mainpanel.setBorder(new EmptyBorder(30,30,30,30));
		mainpanel.setLayout(new GridLayout(5,1));
		initialization();
	}
	public void initialization(){
		studentlogin = new JButton("Student Login");
		gueststudentlogin = new JButton("Guest Student Login");
		professorlogin = new JButton("Professor Login");
		employerlogin = new JButton("Employer Login");
		
		try {
			image = ImageIO.read(this.getClass().getResource("login.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loginlabel = new JLabel(){
			 @Override
	         protected void paintComponent(Graphics g) {
	             super.paintComponent(g);
	             g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(),null);
			 }
		};
		createGui();
	}
	public void createGui(){
		add(mainpanel);
		mainpanel.add(loginlabel);
		mainpanel.add(studentlogin);
		mainpanel.add(gueststudentlogin);
		mainpanel.add(professorlogin);
		mainpanel.add(employerlogin);
		setVisible(true);
	}
}
