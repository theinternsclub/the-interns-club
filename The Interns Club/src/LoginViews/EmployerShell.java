package LoginViews;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import client.EmployerProfile;

public class EmployerShell extends JFrame {

	JPanel main;
	JToolBar Empshell;
	JButton homeButton;
	JButton uploadButton;
	JButton viewButton;
	JButton logoutButton;
	BufferedImage homeImage;
	JobDescriptionPanel jobpanel;
	EmployerProfile profilePanel;

	public EmployerShell() {
		super("The Interns Club");
		setSize(500, 500);
		setLocation(100, 100);
		setResizable(false);
		initialization();
		actionlisteners();
	}

	public void initialization() {
		jobpanel = new JobDescriptionPanel();
		profilePanel = new EmployerProfile();
		main = new JPanel();
		main.setLayout(new CardLayout());
		main.setBackground(Color.white);
		Empshell = new JToolBar();
		// Empshell.setBackground(Color.white);
		Empshell.setFloatable(false);
		ImageIcon homeimage = new ImageIcon(this.getClass().getResource("home.png"));
		Image dum = homeimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		homeimage = new ImageIcon(dum);
		homeButton = new JButton(homeimage);
		homeButton.setPreferredSize(new Dimension(50, 30));
		homeButton.setToolTipText("Home");
		ImageIcon uploadimage = new ImageIcon(this.getClass().getResource("upload.png"));
		Image dum1 = uploadimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		uploadimage = new ImageIcon(dum1);
		uploadButton = new JButton(uploadimage);
		uploadButton.setPreferredSize(new Dimension(50, 30));
		uploadButton.setToolTipText("Upload Job");
		ImageIcon viewimage = new ImageIcon(this.getClass().getResource("view.png"));
		Image dum2 = viewimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		viewimage = new ImageIcon(dum2);
		viewButton = new JButton(viewimage);
		viewButton.setPreferredSize(new Dimension(50, 30));
		viewButton.setToolTipText("View Jobs");
		ImageIcon logoutimage = new ImageIcon(this.getClass().getResource("logout.png"));
		Image dum3 = logoutimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		logoutimage = new ImageIcon(dum3);
		logoutButton = new JButton(logoutimage);
		logoutButton.setPreferredSize(new Dimension(50, 30));
		logoutButton.setToolTipText("Log out");
		CreateGui();
	}

	public void CreateGui() {
		Empshell.add(homeButton);
		Empshell.add(Box.createHorizontalStrut(10));
		Empshell.add(uploadButton);
		Empshell.add(Box.createHorizontalStrut(10));
		Empshell.add(viewButton);
		Empshell.add(Box.createGlue());
		Empshell.add(logoutButton);
		// main.add(jobpanel);
		main.add(profilePanel);
		add(Empshell, BorderLayout.NORTH);
		add(main, BorderLayout.CENTER);
		setVisible(true);
	}

	public void actionlisteners() {
		homeButton.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {

			}
		});
	}

}
