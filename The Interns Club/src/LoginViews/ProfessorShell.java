package LoginViews;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import client.ProfessorProfile;

public class ProfessorShell extends JFrame {
	JPanel main;
	JToolBar Profshell;
	JButton homeButton;
	JButton viewButton;
	JButton logoutButton;
	BufferedImage homeImage;

	ProfessorProfile professorProfile;

	public ProfessorShell() {
		super("The Interns Club");
		setSize(500, 500);
		setLocation(100, 100);
		setResizable(false);
		initialization();
	}

	public void initialization() {
		main = new JPanel();
		professorProfile = new ProfessorProfile();
		main.setBackground(Color.white);
		main.add(professorProfile);
		main.setLayout(new CardLayout());
		Profshell = new JToolBar();
		// Empshell.setBackground(Color.white);
		Profshell.setFloatable(false);
		ImageIcon homeimage = new ImageIcon(this.getClass().getResource("home.png"));
		Image dum = homeimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		homeimage = new ImageIcon(dum);
		homeButton = new JButton(homeimage);
		homeButton.setPreferredSize(new Dimension(50, 30));
		homeButton.setToolTipText("Home");
		ImageIcon viewimage = new ImageIcon(this.getClass().getResource("view.png"));
		Image dum2 = viewimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		viewimage = new ImageIcon(dum2);
		viewButton = new JButton(viewimage);
		viewButton.setPreferredSize(new Dimension(50, 30));
		viewButton.setToolTipText("Recommend Students");
		ImageIcon logoutimage = new ImageIcon(this.getClass().getResource("logout.png"));
		Image dum3 = logoutimage.getImage().getScaledInstance(50, 30, java.awt.Image.SCALE_SMOOTH);
		logoutimage = new ImageIcon(dum3);
		logoutButton = new JButton(logoutimage);
		logoutButton.setPreferredSize(new Dimension(50, 30));
		logoutButton.setToolTipText("Log out");
		CreateGui();
	}

	public void CreateGui() {
		Profshell.add(homeButton);
		Profshell.add(Box.createHorizontalStrut(10));
		Profshell.add(viewButton);
		Profshell.add(Box.createGlue());
		Profshell.add(logoutButton);
		add(Profshell, BorderLayout.NORTH);
		add(main, BorderLayout.CENTER);
		setVisible(true);
	}

	public void actionlisteners() {

	}
}
