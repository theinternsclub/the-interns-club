package users;

public class Employer extends User {

	public Employer(String username) {
		super(username);
		// TODO Auto-generated constructor stub
	}

	String Company, Email, School, Phone, Logo, desc;

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getLogo() {
		return Logo;
	}

	public void setLogo(String logo) {
		Logo = logo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getSchool() {
		return School;
	}

	public void setSchool(String school) {
		School = school;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public void updateJobs() {
		// TODO
	}

	public void createJob() {
		// TODO create job and send to server
	}
}
