package users;

public class Professor extends User {

	public Professor(String username) {
		super(username);
		// TODO Auto-generated constructor stub
	}

	String FirstName, LastName, Email, School, Phone;

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getSchool() {
		return School;
	}

	public void setSchool(String school) {
		School = school;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public void sendNotification() {
		// TODO
	}

	public void updateJobs() {
		// TODO
	}

	public void recommendJobs() {
		// TODO
	}

	public void currentJobList(/* JobList */) {
		// TODO
	}

}
