package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class EmployerProfile extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton save, edit, newImage;
	private JLabel logo, bioLabel, joinDate, contact;
	private JTextField name;
	private JTextArea bio;
	private JPanel bottomPanel, editPanel;

	public EmployerProfile() {
		initComponents();
		createGUI();
		addListeners();
	}

	public void initComponents() {
		save = new JButton("Save");
		edit = new JButton("Edit Profile");

		newImage = new JButton("Upload New Logo");

		bioLabel = new JLabel("Bio: ");
		bioLabel.setAlignmentX(LEFT_ALIGNMENT);

		name = new JTextField("Company Name");
		name.setPreferredSize(new Dimension(150, 40));
		name.setMinimumSize(new Dimension(150, 40));
		name.setMaximumSize(new Dimension(150, 40));
		name.setEditable(false);

		joinDate = new JLabel("Join Date: 1/1/2001");
		joinDate.setAlignmentX(LEFT_ALIGNMENT);
		joinDate.setHorizontalAlignment(SwingConstants.LEFT);

		contact = new JLabel("Contact: hello@google.com");
		contact.setAlignmentX(LEFT_ALIGNMENT);
		contact.setHorizontalAlignment(SwingConstants.LEFT);

		bio = new JTextArea("Bio Goes Here");
		bio.setPreferredSize(new Dimension(500, 200));
		bio.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		bio.setEditable(false);
		bio.setWrapStyleWord(true);
		bio.setLineWrap(true);
	}

	public void createGUI() {
		setSize(500, 470);
		setMaximumSize(new Dimension(500, 450));
		setMinimumSize(new Dimension(500, 450));
		setBackground(Color.WHITE);

		JPanel mainArea = new JPanel();
		mainArea.setLayout(new BoxLayout(mainArea, BoxLayout.Y_AXIS));
		mainArea.setBackground(Color.white);

		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));

		ImageIcon noImg = new ImageIcon("images/google.jpg");
		Image dum1 = noImg.getImage().getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		noImg = new ImageIcon(dum1);
		logo = new JLabel(noImg);
		logo.setPreferredSize(new Dimension(150, 150));

		JPanel nameDate = new JPanel();

		nameDate.setLayout(new BorderLayout());
		nameDate.setBackground(Color.white);
		nameDate.add(name, BorderLayout.NORTH);
		nameDate.add(contact);
		nameDate.add(joinDate, BorderLayout.SOUTH);
		nameDate.setMaximumSize(new Dimension(300, 150));
		topPanel.add(Box.createHorizontalStrut(40));
		topPanel.add(logo);
		topPanel.add(Box.createHorizontalStrut(10));

		topPanel.add(nameDate);

		editPanel = new JPanel();
		editPanel.setBackground(Color.white);
		editPanel.add(edit);

		topPanel.add(editPanel);

		topPanel.setBackground(Color.WHITE);

		mainArea.add(topPanel);
		mainArea.setBackground(Color.white);

		JPanel bioLabelPanel = new JPanel();
		bioLabelPanel.setPreferredSize(new Dimension(500, 20));
		bioLabelPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 463));
		bioLabelPanel.add(bioLabel);
		bioLabelPanel.setBackground(Color.WHITE);

		mainArea.add(bioLabelPanel);

		JPanel bioPanel = new JPanel();
		bioPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		bioPanel.setBackground(Color.WHITE);
		bioPanel.add(bio);

		mainArea.add(bioPanel);

		bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(1, 2));
		bottomPanel.setPreferredSize(new Dimension(500, 20));
		bottomPanel.add(save);
		bottomPanel.add(newImage);
		bottomPanel.setBackground(Color.white);
		bottomPanel.setVisible(false);

		mainArea.add(bottomPanel);
		add(mainArea);

	}

	public void addListeners() {
		edit.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				editPanel.setVisible(false);
				bio.setEditable(true);
				name.setEditable(true);
				bottomPanel.setVisible(true);
			}
		});

		save.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				editPanel.setVisible(true);
				bio.setEditable(false);
				name.setEditable(false);
				bottomPanel.setVisible(false);

			}
		});
	}
}
