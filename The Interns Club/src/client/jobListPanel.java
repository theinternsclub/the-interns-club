package client;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class jobListPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4900999543053797197L;
	
	private int numOfJobs;
	private JPanel jobButtonBox;
	private Vector<JButton> jobs;
	private Vector<jobDetailsPanel> jdp;
	
	
	public jobListPanel (int numOfJobs){
		this.numOfJobs = numOfJobs;
		initializeVar();
		createGUI();
		addEvents();
	}
	
	private void showJobDetails(jobDetailsPanel toReplace){
		this.removeAll();
		this.add(toReplace);
		this.validate();
		this.repaint();
	}
	
	public void showJobList(){
		this.removeAll();
		this.add(jobButtonBox);
		this.validate();
		this.repaint();
	}
	
	private void initializeVar(){
		jobButtonBox = new JPanel();
		jobButtonBox.setLayout(new GridLayout(numOfJobs,1));
		jdp = new Vector<jobDetailsPanel>();
		jobs = new Vector<JButton>();
		for(int i=0;i<numOfJobs;i++){
			jdp.add(new jobDetailsPanel(this,"Job " + i));
			JButton toAdd = new JButton();
			toAdd.setSize(new Dimension(800,100));
			toAdd.setText("Job " + i);
			toAdd.setFont(new Font("Palatino", Font.BOLD,16));
			jobs.addElement(toAdd);
		}
	}
	
	private void createGUI(){
		setSize(new Dimension(1200,600));
		setVisible(true);
		setLayout(new GridLayout(1,1));
		for(int i=0;i<numOfJobs;i++){
			jobButtonBox.add(jobs.get(i));
		}
		JScrollPane scrollPane = new JScrollPane(jobButtonBox);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane);
	}
	
	private void addEvents(){
		for(int i=0;i<numOfJobs;i++){
			int index =i;
			jobs.get(index).addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					showJobDetails(jdp.get(index));
					
				}
				
			});
		}
	}
	
	public static void main (String[] args){
		JFrame test = new JFrame();
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		test.setSize(800, 600);
		test.add(new jobListPanel(5));
		test.setVisible(true);
	}
	
}
