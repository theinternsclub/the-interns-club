package client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class StudentProfile extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton save, edit, newImage;
	private JLabel profilePicture, bioLabel, joinDate, university, major, GPA;
	private JTextField name;
	private JTextArea bio;
	private JPanel bottomPanel, editPanel;

	public StudentProfile() {
		initComponents();
		createGUI();
		addListeners();
	}

	public void initComponents() {
		save = new JButton("Save");
		edit = new JButton("Edit Profile");

		newImage = new JButton("Upload New Picture");

		bioLabel = new JLabel("Bio: ");
		bioLabel.setAlignmentX(LEFT_ALIGNMENT);

		name = new JTextField("Student Name");

		name.setEditable(false);

		joinDate = new JLabel("Join Date: 1/1/2001");
		joinDate.setAlignmentX(LEFT_ALIGNMENT);
		joinDate.setHorizontalAlignment(SwingConstants.LEFT);

		university = new JLabel("University: USC");
		university.setAlignmentX(LEFT_ALIGNMENT);
		university.setHorizontalAlignment(SwingConstants.LEFT);

		major = new JLabel("Major: Computer Science");
		major.setAlignmentX(LEFT_ALIGNMENT);
		major.setHorizontalAlignment(SwingConstants.LEFT);
		major.setAlignmentX(Component.LEFT_ALIGNMENT);

		GPA = new JLabel("GPA: 4.0");
		GPA.setAlignmentX(LEFT_ALIGNMENT);
		GPA.setHorizontalAlignment(SwingConstants.LEFT);

		bio = new JTextArea("Bio Goes Here");
		bio.setPreferredSize(new Dimension(500, 200));
		bio.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		bio.setEditable(false);
		bio.setWrapStyleWord(true);
		bio.setLineWrap(true);
	}

	public void createGUI() {
		setSize(500, 470);
		setMaximumSize(new Dimension(500, 450));
		setMinimumSize(new Dimension(500, 450));
		setBackground(Color.WHITE);

		JPanel mainArea = new JPanel();
		mainArea.setLayout(new BoxLayout(mainArea, BoxLayout.Y_AXIS));
		mainArea.setBackground(Color.white);

		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));

		ImageIcon noImg = new ImageIcon("images/noimg.jpg");
		Image dum1 = noImg.getImage().getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		noImg = new ImageIcon(dum1);
		profilePicture = new JLabel(noImg);
		profilePicture.setPreferredSize(new Dimension(150, 150));

		JPanel nameDate = new JPanel();

		nameDate.setLayout(new GridLayout(5, 1));
		nameDate.setBackground(Color.white);
		nameDate.add(name);
		nameDate.add(university);
		nameDate.add(major);
		nameDate.add(GPA);
		nameDate.add(joinDate);
		nameDate.setMaximumSize(new Dimension(300, 150));
		nameDate.setAlignmentX(LEFT_ALIGNMENT);

		topPanel.add(Box.createHorizontalStrut(40));
		topPanel.add(profilePicture);
		topPanel.add(Box.createHorizontalStrut(10));

		topPanel.add(nameDate);

		editPanel = new JPanel();
		editPanel.setBackground(Color.white);
		editPanel.add(edit);

		topPanel.add(editPanel);

		topPanel.setBackground(Color.WHITE);

		mainArea.add(topPanel);
		mainArea.setBackground(Color.white);

		JPanel bioLabelPanel = new JPanel();
		bioLabelPanel.setPreferredSize(new Dimension(500, 20));
		bioLabelPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 463));
		bioLabelPanel.add(bioLabel);
		bioLabelPanel.setBackground(Color.WHITE);

		mainArea.add(bioLabelPanel);

		JPanel bioPanel = new JPanel();
		bioPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		bioPanel.setBackground(Color.WHITE);
		bioPanel.add(bio);

		mainArea.add(bioPanel);

		bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(1, 2));
		bottomPanel.setPreferredSize(new Dimension(500, 20));
		bottomPanel.add(save);
		bottomPanel.add(newImage);
		bottomPanel.setBackground(Color.white);
		bottomPanel.setVisible(false);

		mainArea.add(bottomPanel);
		add(mainArea);

	}

	public void addListeners() {
		edit.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				editPanel.setVisible(false);
				bio.setEditable(true);
				name.setEditable(true);
				bottomPanel.setVisible(true);
			}
		});

		save.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				editPanel.setVisible(true);
				bio.setEditable(false);
				name.setEditable(false);
				bottomPanel.setVisible(false);

			}
		});
	}
}
