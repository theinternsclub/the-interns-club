package client;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class jobDetailsPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6833485226695180133L;
	private String jobName;
	private jobListPanel jlp;
	private JComboBox<String> appliedStudents;
	private JTextArea jobDescription;
	private JButton backButton, downloadButton,acceptButton,rejectButton;
	private JLabel studentBio,studentEmail,studentPhone;
	private Image studentImage;
	private JPanel leftPanel,rightPanel;
	public jobDetailsPanel(jobListPanel jlp,String jobName){
		this.jobName = jobName;
		this.jlp = jlp;
		initializeVar();
		createGUI();
		addEvents();
	}
	
	private void initializeVar(){
		File imageFile = new File("images/feelsgoodman.jpg");
		try {
			studentImage = ImageIO.read(imageFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		appliedStudents = new JComboBox<String>();
		for(int i =0;i<5;i++){
			String studentName = "Student " + i;
			appliedStudents.addItem(studentName);
			
		}
		jobDescription = new JTextArea();
		jobDescription.setText(jobName + ": This is a sample text for job description.");
		jobDescription.setEditable(false);
		jobDescription.setFont(new Font("Palatino", Font.BOLD,16));
		
		studentBio = new JLabel();
		studentEmail = new JLabel("student@usc.edu");
		studentPhone = new JLabel("123-456-7890");
		
		backButton = new JButton("Back to Job List");
		downloadButton = new JButton("Download Resume");
		acceptButton = new JButton("Accept");
		rejectButton = new JButton("Reject");
	}
	
	private void createGUI(){
		setSize(new Dimension(1200,600));
		setVisible(true);
		setLayout(new GridLayout(1,2));
		leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel,BoxLayout.Y_AXIS));
		leftPanel.add(backButton);
		leftPanel.add(Box.createGlue());
		leftPanel.add(appliedStudents);
		leftPanel.add(Box.createGlue());
		leftPanel.add(jobDescription);
		leftPanel.add(Box.createGlue());
		
		rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel,BoxLayout.Y_AXIS));
		
		JLabel imagePane = new JLabel(new ImageIcon(studentImage.getScaledInstance(200, 200, Image.SCALE_DEFAULT)));
		rightPanel.add(imagePane);
		
		JPanel bioPanel = new JPanel();
		bioPanel.setLayout(new BoxLayout(bioPanel,BoxLayout.X_AXIS));
		bioPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel bio = new JLabel("Bio: ");
		bioPanel.add(bio);
		bioPanel.add(studentBio);
		rightPanel.add(bioPanel);
		
		rightPanel.add(Box.createGlue());
		
		JPanel emailPanel = new JPanel();
		emailPanel.setLayout(new BoxLayout(emailPanel,BoxLayout.X_AXIS));
		emailPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel email = new JLabel("Email: ");
		emailPanel.add(email);
		emailPanel.add(studentEmail);
		rightPanel.add(emailPanel);
		
		rightPanel.add(Box.createGlue());
		
		JPanel phonePanel = new JPanel();
		phonePanel.setLayout(new BoxLayout(phonePanel,BoxLayout.X_AXIS));
		phonePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel phone = new JLabel("Phone: ");
		phonePanel.add(phone);
		phonePanel.add(studentPhone);
		rightPanel.add(phonePanel);
		
		
		rightPanel.add(Box.createGlue());
		
		JPanel buttonBox = new JPanel();
		buttonBox.setLayout(new BoxLayout(buttonBox,BoxLayout.X_AXIS));
		buttonBox.add(acceptButton);
		buttonBox.add(Box.createGlue());
		buttonBox.add(downloadButton);
		buttonBox.add(Box.createGlue());
		buttonBox.add(rejectButton);
		rightPanel.add(buttonBox);
		
		
		add(leftPanel);
		add(rightPanel);
		leftPanel.setVisible(true);
		rightPanel.setVisible(false);
		
		
	}
	
	private void addEvents(){
		appliedStudents.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String studentName = (String)appliedStudents.getSelectedItem();
				studentBio.setText(studentName + "'s bio goes here");
				rightPanel.setVisible(true);
			}
			
		});
		
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				rightPanel.setVisible(false);
				jlp.showJobList();
				
			}
			
		});
	}
}
